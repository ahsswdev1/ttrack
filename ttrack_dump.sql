-- MySQL dump 10.13  Distrib 5.7.8-rc, for Win64 (x86_64)
--
-- Host: localhost    Database: ttrack
-- ------------------------------------------------------
-- Server version	5.7.8-rc-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sap_wbs_number` int(11) DEFAULT NULL,
  `sap_wbs_fields` int(11) DEFAULT NULL,
  `is_inderect` int(11) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` varchar(20) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_hours`
--

DROP TABLE IF EXISTS `project_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `timecard_id` int(11) DEFAULT NULL,
  `mdtdirectorykey` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `hours_worked_sat` int(11) DEFAULT NULL,
  `hours_worked_sun` int(11) DEFAULT NULL,
  `hours_worked_mon` int(11) DEFAULT NULL,
  `hours_worked_tues` int(11) DEFAULT NULL,
  `hours_worked_wed` int(11) DEFAULT NULL,
  `hours_worked_thurs` int(11) DEFAULT NULL,
  `hours_worked_fri` int(11) DEFAULT NULL,
  `hours_worked_total` int(11) DEFAULT NULL,
  `is_inderect` int(11) DEFAULT NULL,
  `adjustment_key` int(11) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id-hours_idx` (`project_id`),
  KEY `timecard_id-hours_idx` (`timecard_id`),
  KEY `task_id-hours_idx` (`task_id`),
  CONSTRAINT `project_id-hours` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `task_id-hours` FOREIGN KEY (`task_id`) REFERENCES `project_task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timecard_id-hours` FOREIGN KEY (`timecard_id`) REFERENCES `timecard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_hours`
--

LOCK TABLES `project_hours` WRITE;
/*!40000 ALTER TABLE `project_hours` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_manager`
--

DROP TABLE IF EXISTS `project_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_manager` (
  `project_id` int(11) NOT NULL,
  `mdtdirectorykey` int(11) NOT NULL DEFAULT '0',
  `role` varchar(45) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`,`mdtdirectorykey`),
  CONSTRAINT `id-manager` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_manager`
--

LOCK TABLES `project_manager` WRITE;
/*!40000 ALTER TABLE `project_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Project_id` int(11) DEFAULT NULL,
  `task_discription` varchar(100) DEFAULT NULL,
  `task_label` varchar(20) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id-task_idx` (`Project_id`),
  CONSTRAINT `id-task` FOREIGN KEY (`Project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_workers`
--

DROP TABLE IF EXISTS `project_workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_workers` (
  `project_id` int(11) NOT NULL,
  `mdtdirectorykey` int(11) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  `fg_work_order_id` int(11) DEFAULT NULL,
  `dir_employeetype` int(11) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`,`mdtdirectorykey`),
  CONSTRAINT `id-worker` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_workers`
--

LOCK TABLES `project_workers` WRITE;
/*!40000 ALTER TABLE `project_workers` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_workers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timecard`
--

DROP TABLE IF EXISTS `timecard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timecard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mdtdirectorykey` int(11) DEFAULT NULL,
  `week_worked` int(11) DEFAULT NULL,
  `submit_date` int(11) DEFAULT NULL,
  `is_submitted` int(11) DEFAULT NULL,
  `is_approved` int(11) DEFAULT NULL,
  `is_sent_to_sap` int(11) DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_user` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timecard`
--

LOCK TABLES `timecard` WRITE;
/*!40000 ALTER TABLE `timecard` DISABLE KEYS */;
/*!40000 ALTER TABLE `timecard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timecard_dicisions`
--

DROP TABLE IF EXISTS `timecard_dicisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timecard_dicisions` (
  `id` int(11) NOT NULL,
  `timecard_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `approver_dirkey` int(11) DEFAULT NULL,
  `decision` int(11) DEFAULT NULL,
  `decision_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id-tc_decisions_idx` (`timecard_id`),
  CONSTRAINT `project_id-tc_decisions` FOREIGN KEY (`timecard_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `timecard_id-tc_decisions` FOREIGN KEY (`timecard_id`) REFERENCES `timecard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timecard_dicisions`
--

LOCK TABLES `timecard_dicisions` WRITE;
/*!40000 ALTER TABLE `timecard_dicisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `timecard_dicisions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-15 14:14:35
